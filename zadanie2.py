import sys
import random

def generate_sorted_array(size):
    return sorted([random.randint(0, 100) for _ in range(size)])

def binary_search(arr, target):
    left, right = 0, len(arr) - 1

    while left <= right:
        mid = (left + right) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            left = mid + 1
        else:
            right = mid - 1

    return -1

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Шаблон команды: docker run zadanie2 <искомое число>")
        sys.exit(1)

    target = int(sys.argv[1])
    arr = generate_sorted_array(100)
    print("Сгенерированный отсортированный массив:", arr)
    index = binary_search(arr, target)

    if index != -1:
        print(f"Искомое значение {target} найдено в массиве под индексом: {index}")
    else:
        print(f"Искомое значение {target} не найденооаауоао")
