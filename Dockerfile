FROM python:3.11.8-slim

WORKDIR /app

COPY zadanie2.py .

ENTRYPOINT ["python", "zadanie2.py"]